from unittest import TestCase

from polynomial import Polynomial
from coefficient.float import FloatPolynomialCoefficient


class TestPolynomial(TestCase):
    def test_sum_float(self):
        a = Polynomial({2: FloatPolynomialCoefficient(10), 1: FloatPolynomialCoefficient(-1), 0: FloatPolynomialCoefficient(-100)})
        b = Polynomial({100: FloatPolynomialCoefficient(-100), 2: FloatPolynomialCoefficient(-2), 1: FloatPolynomialCoefficient(1), 0: FloatPolynomialCoefficient(1)})
        result = Polynomial({100: FloatPolynomialCoefficient(-100), 2: FloatPolynomialCoefficient(8), 0: FloatPolynomialCoefficient(-99)})

    def test_diff(self):
        self.fail()

    def test_mult(self):
        self.fail()

    def test_div(self):
        self.fail()

    def test_gcd(self):
        self.fail()

    def test_is_greater(self):
        self.fail()

    def test_is_equal(self):
        self.fail()

    def test_kroneker(self):
        self.fail()

    def test_to_str(self):
        self.fail()
