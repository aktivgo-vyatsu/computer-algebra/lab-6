from .coefficient import PolynomialCoefficient


class FloatPolynomialCoefficient(PolynomialCoefficient):

    def __init__(self, value: float):
        self.value = value

    def __add__(self, other):
        return FloatPolynomialCoefficient(round(self.value + other.value, 2))

    def __sub__(self, other):
        return FloatPolynomialCoefficient(round(self.value - other.value, 2))

    def __mul__(self, other):
        return FloatPolynomialCoefficient(round(self.value * other.value, 2))

    def __truediv__(self, other):
        return FloatPolynomialCoefficient(self.value / other.value)

    def __mod__(self, other):
        return FloatPolynomialCoefficient(self.value % other.value)

    def __neg__(self):
        return FloatPolynomialCoefficient(-self.value)

    def __eq__(self, other):
        return self.value == other