from polynomial.coefficient.coefficient import PolynomialCoefficient


class DeductionPolynomialCoefficient(PolynomialCoefficient):
    def __init__(self, value: int, module: int):
        self.value = value % module
        self.module = module

    def __add__(self, other):
        return DeductionPolynomialCoefficient((self.value + other.value + self.module) % self.module, self.module)

    def __sub__(self, other):
        return DeductionPolynomialCoefficient((self.value - other.value + self.module) % self.module, self.module)

    def __mul__(self, other):
        return DeductionPolynomialCoefficient((self.value * other.value + self.module) % self.module, self.module)

    def __truediv__(self, other):
        return DeductionPolynomialCoefficient((self.value / other.value + self.module) % self.module, self.module)

    def __mod__(self, other):
        return DeductionPolynomialCoefficient((self.value % other.value + self.module) % self.module, self.module)

    def __neg__(self):
        return DeductionPolynomialCoefficient((-self.value + self.module) % self.module, self.module)

    def __eq__(self, other):
        return self.value == other