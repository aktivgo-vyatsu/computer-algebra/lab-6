import itertools
import math

from polynomial.coefficient.float import FloatPolynomialCoefficient


def get_max_degree(coefficients: dict):
    if len(coefficients) == 0:
        return 0
    return max(coefficients.keys())


def clean(cleaned: dict):
    keys = []
    for d, v in cleaned.items():
        if v == 0:
            keys.append(d)
    for k in keys:
        del cleaned[k]


def get_all_dividers(x) -> list:
    result = []
    x = abs(x)
    for i in range(1, int(x / 2) + 1):
        if x % i == 0:
            result.append(int(i))
            result.append(int(-i))
    result.append(int(x))
    result.append(int(-x))
    return result


def create_lagrange(xs, ys):
    result = Polynomial({0: FloatPolynomialCoefficient(0)})
    for i in range(len(ys)):
        result += Polynomial({0: FloatPolynomialCoefficient(ys[i])}) * create_sub_lagrange(xs, i)
    return result


def create_sub_lagrange(xs, ind):
    result = Polynomial({0: FloatPolynomialCoefficient(1)})
    divider = 1
    for i in range(len(xs)):
        if i == ind:
            continue
        divider *= xs[ind] - xs[i]
        cur_pol = Polynomial({1: FloatPolynomialCoefficient(1)})
        cur_pol.coefficients[0] = FloatPolynomialCoefficient(-xs[i])
        result *= cur_pol
    if divider == 0:
        return Polynomial({0: 0})
    return result / Polynomial({0: FloatPolynomialCoefficient(divider)})


def fft(a, invert=None):
    __fft(a, invert)
    return a


def __fft(a, invert):
    print(a)
    n = len(a)
    if n == 1:
        return

    a0 = list()
    a1 = list()
    i = 0
    j = 0
    while i < n:
        a0.append(a[i])
        a1.append(a[i + 1])
        i += 2
        j += 1

    __fft(a0, invert)
    __fft(a1, invert)

    ang = 2 * math.pi / n * (-1 if invert else 1)
    w = complex(1)
    wn = complex(math.cos(ang), math.sin(ang))
    for i in range(int(math.ceil(n / 2))):
        a[i] = a0[i] + w * a1[i]
        a[i + n // 2] = a0[i] - w * a1[i]
        if invert:
            a[i] /= 2
            a[i + n // 2] /= 2
        w *= wn


class Polynomial:

    def __init__(self, coefficients: dict):
        self.coefficients = coefficients

    def __add__(self, summand):
        result = {}
        degrees = set(list(self.coefficients))
        degrees.update(list(summand.coefficients.keys()))

        for degree in degrees:
            if degree in self.coefficients and degree in summand.coefficients:
                sum = self.coefficients[degree] + summand.coefficients[degree]
                if sum != 0:
                    result[degree] = sum
            elif degree in self.coefficients:
                result[degree] = self.coefficients[degree]
            else:
                result[degree] = summand.coefficients[degree]

        clean(result)
        return Polynomial(result)

    def __sub__(self, subtracted):
        result = {}
        degrees = set(list(self.coefficients))
        degrees.update(list(subtracted.coefficients.keys()))

        for degree in degrees:
            if degree in self.coefficients and degree in subtracted.coefficients:
                diff = self.coefficients[degree] - subtracted.coefficients[degree]
                if diff != 0:
                    result[degree] = diff
            elif degree in self.coefficients:
                result[degree] = self.coefficients[degree]
            else:
                result[degree] = -subtracted.coefficients[degree]

        clean(result)
        return Polynomial(result)

    def __mul__(self, multiplier):
        result = {}

        for d1, v1 in self.coefficients.items():
            for d2, v2 in multiplier.coefficients.items():
                degree = d1 + d2
                value = v1 * v2
                result[degree] = result[degree] + value if degree in result else value

        clean(result)
        return Polynomial(result)

    def __truediv__(self, other):
        result = {}

        self_max_degree = get_max_degree(self.coefficients)
        divider_max_degree = get_max_degree(other.coefficients)

        if self_max_degree < divider_max_degree:
            raise Exception('the divisible cannot be less than the divisor')

        buf = Polynomial(self.coefficients)

        while buf > other and len(buf.coefficients) != 0:
            dk = int(get_max_degree(buf.coefficients) - get_max_degree(other.coefficients))
            vk = buf.coefficients[get_max_degree(buf.coefficients)] / other.coefficients[
                get_max_degree(other.coefficients)]

            result[dk] = vk

            buf = buf - (Polynomial(other.coefficients) * Polynomial({dk: vk}))

        clean(result)
        return Polynomial(result)

    def __mod__(self, other):
        remain = {}

        self_max_degree = get_max_degree(self.coefficients)
        divider_max_degree = get_max_degree(other.coefficients)

        if self_max_degree < divider_max_degree:
            raise Exception('the divisible cannot be less than the divisor')

        buf = Polynomial(self.coefficients)

        while buf > other and len(buf.coefficients) != 0:
            dk = int(get_max_degree(buf.coefficients) - get_max_degree(other.coefficients))
            vk = buf.coefficients[get_max_degree(buf.coefficients)] / other.coefficients[
                get_max_degree(other.coefficients)]

            buf = buf - (Polynomial(other.coefficients) * Polynomial({dk: vk}))

        remain = buf.coefficients

        clean(remain)
        return Polynomial(remain)

    def __divmod__(self, other):
        result, remain = ({}, {})

        self_max_degree = get_max_degree(self.coefficients)
        divider_max_degree = get_max_degree(other.coefficients)

        if self_max_degree < divider_max_degree:
            raise Exception('the divisible cannot be less than the divisor')

        buf = Polynomial(self.coefficients)

        while buf > other and len(buf.coefficients) != 0:
            dk = int(get_max_degree(buf.coefficients) - get_max_degree(other.coefficients))
            vk = buf.coefficients[get_max_degree(buf.coefficients)] / other.coefficients[
                get_max_degree(other.coefficients)]

            result[dk] = vk

            buf = buf - (Polynomial(other.coefficients) * Polynomial({dk: vk}))

        remain = buf.coefficients

        clean(result)
        clean(remain)
        return Polynomial(result), Polynomial(remain)

    def gcd(self, other):
        a = self
        while len(a.coefficients) != 0 and len(other.coefficients) != 0:
            if a > other:
                _, a = divmod(a, other)
            else:
                _, other = divmod(other, a)

        sum = a + other
        result = sum / Polynomial({0: sum.coefficients[get_max_degree(sum.coefficients)]})

        return result

    def __gt__(self, other) -> bool:
        if len(self.coefficients) == 0 or len(other.coefficients) == 0:
            return False
        self_max_degree = get_max_degree(self.coefficients)
        compared_max_degree = get_max_degree(other.coefficients)
        if self_max_degree > compared_max_degree:
            return True
        if self_max_degree == compared_max_degree:
            if abs(self.coefficients[self_max_degree].value) >= abs(
                    other.coefficients[compared_max_degree].value):
                return True
        return False

    def __eq__(self, other) -> bool:
        if len(self.coefficients) != len(other.coefficients):
            return False

        for i in self.coefficients:
            if i not in other.coefficients:
                return False
            if self.coefficients[i] == other.coefficients[i]:
                return False

        return True

    def get_value(self, x):
        result = 0
        for degree, value in self.coefficients.items():
            result += pow(x, degree) * value.value
        return result

    def kroneker(self) -> list:
        result = []

        buffer = Polynomial(self.coefficients)

        end = False
        while not end:
            n = get_max_degree(buffer.coefficients)

            if n == 0:
                return result

            for i in range(1, math.ceil(n / 2) + 1):
                find = False

                m = i
                divisors = []
                for k in range(0, m + 1):
                    value = buffer.get_value(k)

                    if value == 0:
                        polynom = Polynomial({0: FloatPolynomialCoefficient(-k), 1: FloatPolynomialCoefficient(1)})
                        clean(polynom.coefficients)
                        result.append(polynom)
                        buffer = buffer / polynom
                        find = True
                        break

                    divisors.append(get_all_dividers(value))

                if find:
                    break

                print(f'Делители {divisors}')

                U = [u for u in itertools.product(*divisors)]
                print(f'Декартово произведение: {U}')

                print(f'Текущий многочлен {buffer}')

                for y_values in U:
                    x_values = []
                    for j in range(len(y_values)):
                        x_values.append(j)

                    lagrange = create_lagrange(x_values, y_values)

                    if lagrange != Polynomial({}):
                        remain = buffer % lagrange
                        print(f'Многочлен Лагранжа {lagrange} -> остаток {remain}')

                        if remain == Polynomial({}) and len(lagrange.coefficients) == m + 1:
                            lagrange, _ = divmod(
                                lagrange,
                                Polynomial({0: lagrange.coefficients[get_max_degree(lagrange.coefficients)]})
                            )
                            buffer = buffer / lagrange
                            clean(lagrange.coefficients)
                            result.append(lagrange)
                            find = True
                            break

                if find:
                    break

                end = True

        clean(buffer.coefficients)
        result.append(buffer)
        return result

    def __str__(self):
        result = ''
        if len(self.coefficients) == 0:
            return '0'
        for i in sorted(self.coefficients.keys(), reverse=True):
            result += '-' if self.coefficients[i].value < 0 else ''
            if result != '':
                result += '+' if self.coefficients[i].value > 0 else ''
            result += str(abs(self.coefficients[i].value) if abs(self.coefficients[i].value) != 1 or i == 0
                          else '') + ('x' + ('^' + str(i) if i > 1 else '') if i > 0 else '')
        return result
