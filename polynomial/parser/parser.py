from polynomial.coefficient.deduction import DeductionPolynomialCoefficient
from polynomial.coefficient.float import FloatPolynomialCoefficient
from polynomial.parser.params import PolynomialParserParams
from polynomial.polynomial import Polynomial


class PolynomialParser:
    def __init__(self, params: PolynomialParserParams):
        self.polynom = {}
        self.coefficient = 0
        self.c = 10
        self.negative_coefficient = False
        self.degree = 0
        self.params = params

    def __reset(self):
        self.polynom = {}
        self.coefficient = 0
        self.c = 10
        self.negative_coefficient = False
        self.degree = 0

    def __set_or_incr(self, degree: int, value: float):
        if value == 0:
            return
        if degree in self.polynom:
            self.polynom[degree] = self.polynom[degree].sum(self.__create_coefficient(value))
            return
        self.polynom[degree] = self.__create_coefficient(value)

    def __create_coefficient(self, value: float):
        match self.params.mode:
            case self.params.ParserMode.float:
                return FloatPolynomialCoefficient(value)
            case self.params.ParserMode.deduction:
                return DeductionPolynomialCoefficient(int(value), self.params.module)

    def parse(self, input: str) -> Polynomial:
        self.__reset()
        self.__recognize(input)
        return Polynomial(self.polynom)

    def __recognize(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.signs:
                self.negative_coefficient = input[i] == '-'
                return self.__left_sign(input[i + 1:])
            if input[i] in self.params.numbers:
                self.__calculate_left(int(input[i]))
                return self.__left_num(input[i + 1:])
            if input[i] == self.params.comma:
                return self.__comma(input[i + 1:])
            if input[i] in 'x':
                self.coefficient = 1
                return self.__x(input[i + 1:])
            raise Exception()

    def __left_sign(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                self.__calculate_left(int(input[i]))
                return self.__left_num(input[i + 1:])
            if input[i] == self.params.comma:
                return self.__comma(input[i + 1:])
            if input[i] in 'x':
                return self.__x(input[i + 1:])
            raise Exception()
        raise Exception()

    def __left_num(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                self.__calculate_left(int(input[i]))
                continue
            if input[i] == self.params.comma:
                return self.__comma(input[i + 1:])
            if input[i] in 'x':
                return self.__x(input[i + 1:])
            if input[i] in self.params.signs:
                self.__set_or_incr(0, -self.coefficient if self.negative_coefficient else self.coefficient)
                self.negative_coefficient = input[i] == '-'
                self.coefficient = 0
                return self.__right_sign(input[i + 1:])
            raise Exception()
        self.__set_or_incr(0, -self.coefficient if self.negative_coefficient else self.coefficient)
        return self.polynom

    def __comma(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                self.__calculate_right(int(input[i]))
                return self.__right_num(input[i + 1:])
            raise Exception()
        raise Exception()

    def __right_num(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                self.__calculate_right(int(input[i]))
                continue
            if input[i] in 'x':
                self.c = 10
                return self.__x(input[i + 1:])
            if input[i] in self.params.signs:
                self.__set_or_incr(0, -self.coefficient if self.negative_coefficient else self.coefficient)
                self.negative_coefficient = input[i] == '-'
                self.coefficient = 0
                self.c = 10
                return self.__right_sign(input[i + 1:])
            raise Exception()
        self.__set_or_incr(0, -self.coefficient if self.negative_coefficient else self.coefficient)
        return self.polynom

    def __calculate_left(self, input: int):
        self.coefficient = self.coefficient * 10 + input

    def __calculate_right(self, input: int):
        self.coefficient = self.coefficient + input / self.c
        self.c *= 10

    def __x(self, input: str):
        for i in range(len(input)):
            if input[i] in '^':
                return self.__degree(input[i + 1:])
            if input[i] in self.params.signs:
                self.__set_or_incr(1, -self.coefficient if self.negative_coefficient else self.coefficient)
                self.coefficient = 0
                self.negative_coefficient = input[i] == '-'
                return self.__right_sign(input[i + 1:])
            raise Exception()
        self.__set_or_incr(1, -self.coefficient if self.negative_coefficient else self.coefficient)
        return self.polynom

    def __degree(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                self.degree = int(input[i])
                return self.__degree_number(input[i + 1:])
            raise Exception()
        self.__set_or_incr(self.degree, -self.coefficient if self.negative_coefficient else self.coefficient)
        raise Exception()

    def __degree_number(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                self.degree = self.degree * self.c + int(input[i])
                continue
            if input[i] in self.params.signs:
                self.__set_or_incr(self.degree, -self.coefficient if self.negative_coefficient else self.coefficient)
                self.coefficient = 0
                self.negative_coefficient = input[i] == '-'
                self.degree = 0
                return self.__right_sign(input[i + 1:])
            raise Exception()
        self.__set_or_incr(self.degree, -self.coefficient if self.negative_coefficient else self.coefficient)
        return self.polynom

    def __right_sign(self, input: str):
        for i in range(len(input)):
            if input[i] in self.params.numbers:
                self.__calculate_left(int(input[i]))
                return self.__left_num(input[i + 1:])
            if input[i] in 'x':
                self.coefficient = 1
                return self.__x(input[i + 1:])
            raise Exception()
        raise Exception()
