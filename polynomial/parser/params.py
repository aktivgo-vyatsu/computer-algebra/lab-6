import enum


class PolynomialParserParams:
    class ParserMode(enum.Enum):
        float = 0,
        deduction = 1,

    def __init__(self, numbers: list, signs: list, comma: str, mode: ParserMode, module=None):
        self.numbers = numbers
        self.signs = signs
        self.comma = comma

        if mode == self.ParserMode.deduction and module is None:
            raise Exception('module can not be none')

        self.mode = mode
        self.module = module
