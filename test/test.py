from polynomial.parser.params import PolynomialParserParams
from polynomial.parser.parser import PolynomialParser


if __name__ == "__main__":
    params = PolynomialParserParams(
        numbers=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        signs=['-', '+'],
        comma='.',
        mode=PolynomialParserParams.ParserMode.float,
        module=5
    )

    parser = PolynomialParser(params)

    with open('input/polynomials.txt', 'r') as fr:
        for line in (l.rstrip() for l in fr.readlines()):
            try:
                polynom = parser.parse(line)
                print(line, 'is correct:', polynom.to_str())
            except Exception:
                print(line, 'is incorrect')
