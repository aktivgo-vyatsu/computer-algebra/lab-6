from polynomial.parser.params import PolynomialParserParams
from polynomial.parser.parser import PolynomialParser
from polynomial.polynomial import Polynomial
from polynomial.polynomial import fft

if __name__ == "__main__":
    params = PolynomialParserParams(
        numbers=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        signs=['-', '+'],
        comma='.',
        mode=PolynomialParserParams.ParserMode.float,
        module=5
    )

    parser = PolynomialParser(params)
    polynom_left = Polynomial
    polynom_right = Polynomial

    polynom = ''

    try:
        # x - 1
        # x - 2
        # x + 2
        polynom = 'x^3+x^2+x+1'
        #polynom = 'x^2+3x-3'
        #polynom = '4x^3+2x^2+x+3'
        #polynom = '6x^3-18x^2+18x-6'
        polynom_left = parser.parse(polynom)
        print('recognized polynom:', polynom_left)
    except Exception as e:
        print(polynom, 'is incorrect:', e)
        exit()

    try:
        polynom = '3x^2-2x-1'
        polynom_right = parser.parse(polynom)
        print('recognized polynom:', polynom_right)
    except Exception:
        print(polynom, 'is incorrect')
        exit()

    print('sum:', polynom_left + polynom_right)
    print('diff:', polynom_left - polynom_right)
    print('mult:', polynom_left * polynom_right)
    print('is equal:', polynom_left == polynom_right)

    result, remain = divmod(polynom_left, polynom_right)
    print('div result:', result, 'remain:', remain)

    print('gcd:', polynom_left.gcd(polynom_right))

    print('kroneker:')

    try:
        for res in polynom_left.kroneker():
            print(res)
    except Exception as e:
        print(e)

    fft_res = fft([complex(3), complex(1), complex(2), complex(4)], False)
    print('fft:', fft_res)

    reverse_fft_res = fft(fft_res, True)
    print('reverse fft:', reverse_fft_res)
